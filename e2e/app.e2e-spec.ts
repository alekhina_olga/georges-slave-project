import { GeorgesSlaveProjectPage } from './app.po';

describe('georges-slave-project App', function() {
  let page: GeorgesSlaveProjectPage;

  beforeEach(() => {
    page = new GeorgesSlaveProjectPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
