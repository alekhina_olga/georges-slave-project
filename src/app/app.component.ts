import { Component } from '@angular/core';

const LOCAL_STORAGE_KEY = 'users';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public users = [];
  constructor() {
    this.users = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY)) || [];
  }
  public addUser(user: any) {
    this.users.push(user);
    this.updateLs();
  }
  public deleteUser(index: number) {
    this.users.splice(index, 1);
    this.updateLs();
  }
  public updateLs() {
    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(this.users));
  }
}
