import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-form-user',
  templateUrl: './form-user.component.html',
  styleUrls: ['./form-user.component.css']
})
export class FormUserComponent implements OnInit {
  @Output() addUser = new EventEmitter();
  public user = {
      name: '',
      family: '',
      surname: ''
  };

  constructor() { }

  ngOnInit() {
  }

  onButtonClick() {
    this.addUser.emit(this.user);
  }

}
