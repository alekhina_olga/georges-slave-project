import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.css']
})
export class UserCardComponent implements OnInit {
  @Input() user = {};
  @Output() deleteUser = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
  public delUser() {
    this.deleteUser.emit();
  }
}
