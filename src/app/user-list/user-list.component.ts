import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  @Input() users = [];
  @Output() delUser = new EventEmitter();
  public deleteUser(index: number) {
    this.delUser.emit(index);
  }

  constructor() { }

  ngOnInit() {
  }

}
